import numpy as np
import cv2
import sys
import base64

def readb64(uri):
   encoded_data = uri.split(',')[1]
   nparr = np.fromstring(base64.b64decode(encoded_data), np.uint8)
   img = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
   return img

#img = readb64(sys.argv[1])

from fer import FER

print(FER.__version__)
detector = FER()
faces = detector.detect_emotions(img)


print(faces)