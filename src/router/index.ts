import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'

import VideoStream from '../views/VideoStream.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'VideoStream',
    component: VideoStream
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
