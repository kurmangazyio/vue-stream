var app  = require( 'express' )();
var http = require( 'http' ).Server( app );

const server = require("socket.io")(http, {
    cors: {
        origin: "http://localhost:8080",
        methods: ["GET", "POST"]
    }
});

const users = new Map();

function run_python(photo) {
    var procc = require('child_process');
    var res = {
        emotion: null
    }
    
    try {
        const process = procc.spawn("python", ["./python/emotion.py", photo]);

        process.stdout.on("data", data => { 
            console.log(data.toString());
        })

    } catch (err) {
        console.log(err);
    }

    /*const emotion = new Promise((resolve, reject) =>{
        process.stdout.on("data", data =>{ console.log(data); resolve('ascasc'); })
        process.stderr.on("data", reject)
    })

    
    emotion.then((res) => {
        res.emotion = res;
        console.log(res);
    })*/

    return res
}

server.on("connection", (socket) => { 
    // connect & disconnect user
    socket.on('newuser', (data) => { users.set(socket, data.user); });
    socket.on('takephoto', (data) => {  
        var photo = data.photo;
        var response = run_python(photo);
        
        for (const [client, user] of users.entries()) {
            if (user.user = data.user) {
                client.emit('response', response)
            }
        }
        //console.log(photo);
    });
    socket.on("disconnect",  () => { users.delete(socket) });
});

http.listen( 3055, function(){
    console.log( "listening on:3055" );
});